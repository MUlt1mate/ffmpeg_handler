*Установка*

Скопировать код
```
git clone git@bitbucket.org:MUlt1mate/ffmpeg_handler.git
```
Установить зависимости
```
composer install
```

В файле .env прописать логин,пароль,БД,адрес хоста в APP_URL

Настроить корень сайта на папку public/

Запустить миграции
```
php artisan migrate
```

Запуск записи видео
```
php artisan grabbing [url]
```

Интерфейс с сохраненными данными в корне сайта
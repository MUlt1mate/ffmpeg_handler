<?php
/**
 * @author mult1mate
 * Date: 04.04.17
 * Time: 15:52
 */

namespace App;

class StreamGrabber
{
    private $stream;
    private $video_length;
    private $overlap_time;
    private $folder = 'public/storage/video/';

    /**
     * StreamGrabber constructor.
     * @param $stream
     * @param $time
     * @param $overlap_time
     */
    public function __construct($stream, $time, $overlap_time)
    {
        $this->stream = $stream;
        $this->video_length = $time;
        $this->overlap_time = $overlap_time;
    }

    public function grab()
    {

        $array = [
            ['pipe', 'r'],
            ['pipe', 'w'],
            ['pipe', 'w']
        ];
        $output_name = 'stream_' . date('Y-m-d_H:i:s') . '.mp4';

        $cmd = 'ffmpeg -y -i ' . $this->stream . ' -c copy -f mp4 -t ' . $this->video_length . ' ' . $this->folder . $output_name;
        $ffmpeg = proc_open($cmd, $array, $pipes);
        stream_set_blocking($pipes[2], 0);

        $video = new Video();
        $video->file_name = $output_name;
        $time_begin = time();
        $video->time_begin = date('Y-m-d H:i:s', $time_begin);
        $video->time_end = date('Y-m-d H:i:s', $time_begin + $this->video_length);

        while (true) {
            $status = proc_get_status($ffmpeg);
            if (!$status['running']) {
                throw (new \Exception('stream error'));
            }

            $fgets = fgets($pipes[2]);
            if (preg_match('/^frame.*time=(\d\d):(\d\d):(\d\d)/', $fgets, $matches)) {
                $length = $matches[1] * 3600 + $matches[2] * 60 + $matches[3];
                if ($this->overlap_time && ($this->overlap_time <= $length)) {
                    return $video;
                }
            }
        }
    }
}
<?php
/**
 * @author mult1mate
 * Date: 05.04.17
 * Time: 19:27
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

/**
 * Class VideoShows
 * @package App
 * @property int $id
 * @property int $video_id
 * @property string $show_name
 * @property string $time_begin
 * @property string $time_end
 * @mixin \Eloquent
 */
class VideoShows extends Model
{
    protected $table = 'video_shows';

    public static function addInfo($video_id, $show_name, $time_begin, $time_end)
    {
        $chunk = new self();
        $chunk->video_id = $video_id;
        $chunk->show_name = $show_name;
        $chunk->time_begin = $time_begin;
        $chunk->time_end = $time_end;
        return $chunk->save();
    }
}
<?php
/**
 * @author mult1mate
 * Date: 05.04.17
 * Time: 12:45
 */

namespace App\Http\Controllers;


use App\ScheduleClient;
use App\Video;

class UserController extends Controller
{
    public function video()
    {
        $video = Video::with('shows')->get();
        return view('user.video_list', ['video' => $video]);
    }

    public function soapTest()
    {
        $client = new ScheduleClient();
        $result = $client->getShowListByTime('13:00', '16:00');
        echo "<h2>Request</h2>";
        echo "<pre>" . htmlspecialchars($client->getRequest(), ENT_QUOTES) . "</pre>";
        echo "<h2>Response</h2>";
        echo "<pre>" . htmlspecialchars($client->getResponse(), ENT_QUOTES) . "</pre>";

        echo "<h2>Schedule</h2><pre>";
        var_dump($result);
        echo "</pre>";
    }
}
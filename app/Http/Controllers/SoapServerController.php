<?php
/**
 * @author mult1mate
 * Date: 05.04.17
 * Time: 12:05
 */

namespace App\Http\Controllers;


use App\SoapServer;
use Illuminate\Http\Response;

class SoapServerController extends Controller
{
    private $server;
    private $endpoint;

    public function __construct()
    {
        $this->server = new SoapServer();
        $this->endpoint = env('APP_URL') . '/WebServices/Schedule.asmx';
        $this->server->configureWSDL('ffmpeg', $this->endpoint . '?WSDL');

        $this->server->wsdl->addComplexType(
            'Item', // the type's name
            'complexType',
            'struct',
            'all',
            '',
            [
                'id' => ['name' => 'id', 'type' => 'xsd:int'],
                'name' => ['name' => 'name', 'type' => 'xsd:string'],
                'time_begin' => ['name' => 'time_begin', 'type' => 'xsd:string'],
                'time_end' => ['name' => 'time_end', 'type' => 'xsd:string'],
            ]
        );
        $this->server->wsdl->addComplexType(
            'Items',
            'complexType',
            'array',
            '',
            'SOAP-ENC:Array',
            [],
            [
                [
                    'ref' => 'SOAP-ENC:arrayType',
                    'wsdl:arrayType' => 'tns:Item[]'
                ]
            ],
            'tns:Item'
        );
    }

    public function response(Response $response)
    {
        $this->server->register('getInfoByTime',
            ['start_time' => 'xsd:string', 'end_time' => 'xsd:string'],
            ['return' => 'tns:Items'], '', $this->endpoint . '#getInfoByTime');

        $this->server->service(file_get_contents('php://input'));
        $response->header('Content-Type', 'text/xml')->setCharset('ISO-8859-1');
        return $response;
    }
}
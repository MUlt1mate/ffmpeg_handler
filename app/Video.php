<?php
/**
 * @author mult1mate
 * Date: 05.04.17
 * Time: 10:41
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Video
 * @package App
 * @property int $id
 * @property int $channel_id
 * @property string $file_name
 * @property string $time_begin
 * @property string $time_end
 * @property VideoShows[] $shows
 */
class Video extends Model
{
    public function shows()
    {
        return $this->hasMany('App\VideoShows');
    }

    public function getShowsList()
    {
        $str = '';
        foreach ($this->shows as $show) {
            $str .= $show->show_name . ' - ' . $show->time_begin . PHP_EOL;
        }
        return $str;
    }
}
<?php

namespace App\Console\Commands;

use App\ScheduleClient;
use App\StreamGrabber;
use App\VideoShows;
use Illuminate\Console\Command;

class Grabbing extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'grabbing {stream?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Grab and save video stream';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $stream = 'rtmp://54.255.176.172/live/newsnation_360p';
        if ((!empty($this->argument('stream')))) {
            $stream = $this->argument('stream');
        }
        $this->info('Started grabbing from ' . $stream);
        $time = 60 * 60;
        $overlap = $time - (5 * 60);
        if ($overlap < 0) {
            $overlap = 0;
        }
        $grabber = new StreamGrabber($stream, $time, $overlap);
        $schedule = new ScheduleClient();
        do {
            $video = $grabber->grab();
            $video->save();
            $this->info($video->file_name . ' saved');
            $shows = $schedule->getShowListByTime($video->time_begin, $video->time_end);
            foreach ($shows as $show) {
                VideoShows::addInfo($video->id, $show['name'], $show['time_begin'], $show['time_end']);
            }
        } while (true);
    }
}

<?php
/**
 * @author mult1mate
 * Date: 05.04.17
 * Time: 19:07
 */

namespace App;


class ScheduleClient
{
    protected $client;
    protected $endpoint;

    public function __construct()
    {
        $this->endpoint = env('APP_URL') . '/WebServices/Schedule.asmx';
        $this->client = new \nusoap_client($this->endpoint);
    }

    public function request($method, $parameters)
    {
        return $this->client->call($method, $parameters);
    }

    public function getRequest()
    {
        return $this->client->request;
    }

    public function getResponse()
    {
        return $this->client->response;
    }

    public function getShowListByTime($time_begin, $time_end)
    {
        return $this->request(
            'getInfoByTime',
            ['start_time' => $time_begin, 'end_time' => $time_end]
        );
    }
}
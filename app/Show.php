<?php
/**
 * @author mult1mate
 * Date: 05.04.17
 * Time: 12:13
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Show
 * @package App
 * @mixin \Eloquent
 */
class Show extends Model
{
    /**
     * @param string $start_time
     * @param string $end_time
     * @return array
     */
    public static function getInfoByTime($start_time, $end_time)
    {
        //date check is omitted
        $shows = self::where('time_end', '>', $start_time)
            ->where('time_begin', '<', $end_time)->get()->toArray();
        return $shows;
    }
}
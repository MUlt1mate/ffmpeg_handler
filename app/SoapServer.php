<?php
/**
 * @author mult1mate
 * Date: 05.04.17
 * Time: 17:36
 */

namespace App;


class SoapServer extends \soap_server
{
    function invoke_method()
    {
        $this->opData = $this->wsdl->getOperationData($this->methodname);
        $this->methodreturn = call_user_func_array(
            '\\App\\Show::' . $this->methodname,
            array_values($this->methodparams)
        );
    }

}
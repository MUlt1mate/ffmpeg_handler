<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shows', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->date('date');
            $table->time('time_begin');
            $table->time('time_end');
            $table->tinyInteger('channel_id')->nullable();
            $table->timestamps();
        });

        $names = [
            'Amar es Primavera',
            'Amor bravío',
            'April, Come She Will',
            'Classic in the Jurassic: Air Obstacle Race; King and Crystal Live!',
            'Dinosaur Train',
            'Duck Dynasty',
            'Flock and Key',
            'FREEFRM',
            'Hard-Ass Teacher',
            'Hello, My Name Is Cheyenne',
            'Help Wanted',
            'Induckpendence Day',
            'Last Man Standing',
            'Laura',
            'Local Programming',
            'Mañana Es Para Siempre',
            'Mujeres Ambiciosas',
            'Noticiero Univisión Edición Digital',
            'Parenting Bud',
            'Pie Hard',
            'Pit Perfect',
            'Project Mandy',
            'Reba',
            'Telemundo',
            'The Ducket List',
            'UniMas',
            'Univision',
            'Wild Wild Pest',
            'Addicted',
            'How to forget about SOAP and start living',
        ];
        $shows = [];
        $current = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
        $i = 0;
        do {
            $date = date('Y-m-d', $current);
            $time_begin = date('H:i:s', $current);
            $length = rand(20, 80) * 60;
            $current += $length;
            $time_end = date('H:i:s', $current - 1);
            $show = ['name' => $names[$i], 'date' => $date, 'time_begin' => $time_begin, 'time_end' => $time_end];
            $shows[] = $show;
            ++$i;
        } while ((date('Y-m-d') == $date) && isset($names[$i]));

        DB::table('shows')->insert($shows);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shows');
    }
}
